DELIMITER | -- On change le délimiteur
CREATE PROCEDURE get_users_info()

BEGIN
DROP TABLE IF EXISTS users_info;
CREATE TEMPORARY  TABLE users_info(
   utilisateurs int,
   actifs int,
   inactifs int,
   super_administrateurs int,
   administrateurs int,
   moderateurs int,
   membres int
);

INSERT INTO users_info VALUES (
    (SELECT count(*) FROM member),
    (SELECT count(*) FROM member WHERE actif = true),
    (SELECT count(*) FROM member WHERE actif = false),
    (SELECT count(*) FROM user_roles JOIN role ON role.id = user_roles.roles_id WHERE role.libelle ='SUPER_ADMIN'),
	(SELECT count(*)-(SELECT count(*) FROM user_roles JOIN role ON role.id = user_roles.roles_id WHERE role.libelle ='SUPER_ADMIN') FROM user_roles JOIN role ON role.id = user_roles.roles_id WHERE role.libelle ='ADMIN'),
	(SELECT count(*)-(SELECT count(*) FROM user_roles JOIN role ON role.id = user_roles.roles_id WHERE role.libelle ='SUPER_ADMIN')
-(SELECT count(*)-(SELECT count(*) FROM user_roles JOIN role ON role.id = user_roles.roles_id WHERE role.libelle ='SUPER_ADMIN')   FROM user_roles JOIN role ON role.id = user_roles.roles_id WHERE role.libelle ='ADMIN'
) FROM user_roles JOIN role ON role.id = user_roles.roles_id WHERE role.libelle ='MOD'),
	(select count(*)
-(select count(*) from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='SUPER_ADMIN')
-(select count(*)-(select count(*) from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='SUPER_ADMIN')   from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='ADMIN')
-(select count(*)-(select count(*) from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='SUPER_ADMIN')
-(select count(*)-(select count(*) from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='SUPER_ADMIN')   from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='ADMIN'
) from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='MOD')
from user_roles JOIN role on role.id = user_roles.roles_id where role.libelle ='USER'));

    SELECT * FROM users_info;
END |

DELIMITER ;
