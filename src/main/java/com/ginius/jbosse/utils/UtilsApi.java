package com.ginius.jbosse.utils;

import com.ginius.jbosse.models.Role;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class UtilsApi {


    private static final Logger logger = Logger.getLogger(UtilsApi.class);

    /**
     * Fonction retournant une liste générée par la récupération d'une énumération
     * d'un fichier text.
     *
     * @param path chemin du fichier d'énumération.
     * @return liste générée.
     */
    public static List<String> generationList(String path) {

        logger.debug("Tentative de récupération d 'une énumération.");

        List<String> alListGen = new ArrayList<>();

        try (Scanner scan = new Scanner(new File(path),"utf8")) {

            while (scan.hasNext()) {
                alListGen.add(scan.next());
            }

            logger.debug("Récupération d 'une énumération réussie.");

            return alListGen;

        } catch (FileNotFoundException e) {

            logger.debug("Erreur lors de la récupération d 'une énumération " + e.getMessage());

            return Collections.emptyList();
        }

    }

    /**
     * Méthode privée qui génère la liste des rôles d'un utilisateur
     *
     * @param userRoles roles de l'utilisateur
     * @return collection de roles de l'utilisateur
     */
    public static Collection<Role> generationUserRoles(Collection<Role> userRoles, Collection<Role> rolesInBase) {

        Collection<Role> rolesFinal;
        Role role = new Role();

        logger.debug("attribution des rôles à l'utilisateur");

        for (Role uRole : userRoles) {
            role = uRole;
        }

        switch (role.getLibelle()) {
            case "SUPER_ADMIN":
                return rolesInBase;
            case "ADMIN":
                return rolesInBase.stream()
                        .filter(r -> !r.getLibelle().equals("SUPER_ADMIN"))
                        .collect(Collectors.toList());
            case "MOD":
                return rolesInBase.stream()
                        .filter(r -> !r.getLibelle().equals("SUPER_ADMIN"))
                        .filter(r -> !r.getLibelle().equals("ADMIN"))
                        .collect(Collectors.toList());

            default:
                return userRoles;

        }
    }

}
