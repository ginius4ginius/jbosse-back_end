package com.ginius.jbosse.utils.exception;

public class AnecdoteNoteFoundException extends RuntimeException {

    public AnecdoteNoteFoundException(String parameter) {
        super("Impossible de trouver l' anecdote : " + parameter);
    }

    public AnecdoteNoteFoundException(String parameter, String message) {
        super(message + parameter);
    }

    public AnecdoteNoteFoundException(long parameter) {
        super("Impossible de trouver l' anecdote  avec id : " + parameter);
    }

}
