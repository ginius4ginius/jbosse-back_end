package com.ginius.jbosse.utils.exception;

public class RoleNotFoundException  extends RuntimeException {

    public RoleNotFoundException(String parameter) {
        super("Impossible de trouver le role : " + parameter);
    }

    public RoleNotFoundException(String parameter, String message) {
        super(message + parameter);
    }

    public RoleNotFoundException(long parameter) {
        super("Impossible de trouver le role avec id : " + parameter);
    }

}
