package com.ginius.jbosse.utils.exception;

public class CategoryNotFoundException extends RuntimeException {

    public CategoryNotFoundException(String parameter) {
        super("Impossible de trouver le métier : " + parameter);
    }

    public CategoryNotFoundException(String parameter, String message) {
        super(message + parameter);
    }

    public CategoryNotFoundException(long parameter) {
        super("Impossible de trouver le métier avec id : " + parameter);
    }

}
