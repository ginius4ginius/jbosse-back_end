package com.ginius.jbosse.utils.exception;

public class UserNotFoundException  extends RuntimeException {

    public UserNotFoundException(String parameter) {
        super("Impossible de trouver l'utilisateur : " + parameter);
    }

    public UserNotFoundException(String parameter, String message) {
        super(message + parameter);
    }
}
