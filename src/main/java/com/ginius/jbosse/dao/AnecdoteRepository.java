package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.anecdote.Anecdote;
import com.ginius.jbosse.models.anecdote.AnecdoteCategory;
import com.ginius.jbosse.models.anecdote.status.AnecdoteStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface AnecdoteRepository extends JpaRepository<Anecdote, Long>, PagingAndSortingRepository<Anecdote, Long> {

	/**
	 * Méthode qui retourne des anecdotes via une pagination
	 * @return list
	 */
	@Query(value ="select x from Anecdote x where x.active = TRUE")
	List<Anecdote> getActiveAnecdotes(Pageable paging);

	List<Anecdote> getAllByCategoryAndActiveIsTrue(AnecdoteCategory cat);

	List<AnecdoteStatus> findAllByActiveIsTrue();

	/**
	 * Méthode qui retourne des anecdotes classé par notation
	 * @return list
	 */
	@Query(value ="select * from anecdote where id in " +
			"(select anecdote_id from notation_anecdote where status_id = " +
			"(select id from anecdote_status where label ='FUN' ) " + " AND active = true " +
			"group by anecdote_id order by count(anecdote_id) DESC)", nativeQuery = true)
	List<Anecdote> getBestAnecdotes(Pageable paging); //méthode à implémenter
}
