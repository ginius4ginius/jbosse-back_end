package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface RoleRepository extends JpaRepository<Role, Long> {

    /**
     * Retoune le role par son libelle
     * La contrainte d'unicité par libelle étant mis en place dans la BDD
     * @param libelle libelle du role
     * @return Role
     */
    Optional<Role> findRoleByLibelle(String libelle);

}
