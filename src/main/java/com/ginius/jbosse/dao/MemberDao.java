package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.UsersInfo;

/**
 * Interface de requêtes personnalisées sortant du cadre des méthodes JPA
 */
public interface MemberDao {

    UsersInfo getUsersInfo();

}
