package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.comment.status.CommentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface CommentStatusRepository extends JpaRepository<CommentStatus, Long> {

    Optional<CommentStatus> getCommentStatusByLabel(String label);

}
