package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.anecdote.status.NotationAnecdote;
import com.ginius.jbosse.models.comment.status.NotationComment;
import com.ginius.jbosse.models.comment.status.NotationCommentPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface NotationCommentRepository extends JpaRepository<NotationComment, NotationCommentPK> {

    /**
     * Méthode qui retourne des commentaires noté par l'utilisateur
     * @param userEmail email de l'utilisateur
     * @param commentId id du commentaire
     * @return liste des notations
     */
    List<NotationComment> findAllByUserEmailAndCommentId(String userEmail, long commentId);

    List<NotationComment> findAllByCommentIdAndStatusId(long commentId, long statusId);
}
