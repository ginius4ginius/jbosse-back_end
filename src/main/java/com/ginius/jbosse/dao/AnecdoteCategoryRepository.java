package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.anecdote.AnecdoteCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AnecdoteCategoryRepository extends JpaRepository<AnecdoteCategory, Long> {

    /**
     * Retourne une catégorie par son label
     * @param label nom du métier
     * @return catégorie
     */
    AnecdoteCategory findAnecdoteCategoryByLabel(String label);
}
