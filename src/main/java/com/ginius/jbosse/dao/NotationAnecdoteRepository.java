package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.anecdote.status.NotationAnecdote;
import com.ginius.jbosse.models.anecdote.status.NotationAnecdotePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface NotationAnecdoteRepository extends JpaRepository<NotationAnecdote, NotationAnecdotePK> {

    /**
     * Méthode qui retourne des commentaires noté par l'utilisateur
     * @param userEmail email de l'utilisateur
     * @param anecdoteId id de l'anedote
     * @return liste des notations
     */
    List<NotationAnecdote> findAllByUserEmailAndAnecdoteId(String userEmail, long anecdoteId);

    List<NotationAnecdote> findAllByAnecdoteIdAndStatusId(long anecdoteId, long statusId);
}
