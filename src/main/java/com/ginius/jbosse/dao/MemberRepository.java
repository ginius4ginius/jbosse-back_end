package com.ginius.jbosse.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.ginius.jbosse.models.Member;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface MemberRepository extends JpaRepository<Member, Long>, MemberDao {

    /**
     * Retoune le premier utilisateur par son username
     * La contrainte d'unicité par username étant mis en place dans la BDD
     * @return Member
     */
    Optional<Member> findUserByUsername(String username);

    /**
     * Retoune les utilisateurs inactifs
     * @return List Users     */
    @RestResource(path = "/inactifUsers")
    List<Member> findAllByActifIsFalse();

    /**
     * Retoune les utilisateurs actifs
     * @return List Users     */
    @RestResource(path = "/actifUsers")
    List<Member> findAllByActifIsTrue();

    /**
     * Retoune les utilisateurs par mc de leur nom
     * @return List Users     */
    @RestResource(path = "/usersByKeyword")
    List<Member> findByNomContains(@Param("mc") String mc);

    /**
     * Méthode permettant de récupérer les utilisateurs par mot clé
     * @param mc mot clé
     * @param pageable la gestion de la pagination
     * @return objet page
     */
    @Query(value = "Select u from Member u where u.username like :x order by u.username")
    Page<Member> checkUsers(@Param("x")String mc, Pageable pageable);

    /**
     * Retoune un utilisateur par son adresse email
     * @return User
     */
    Optional<Member> findUserByEmail(String email);


}
