package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.anecdote.status.AnecdoteStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface AnecdoteStatusRepository extends JpaRepository<AnecdoteStatus, Long> {

    Optional<AnecdoteStatus> getAnecdoteStatusByLabel(String label);

}
