package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.UsersInfo;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MemberDaoImpl implements MemberDao {

    private final Logger logger = Logger.getLogger(MemberDaoImpl.class);

    public final JdbcTemplate jdbcTemplate;

    public MemberDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Méthode de récupération de données générales de l'état des utilisateurs par appel d'une processure stockée
     * @return UsersInfo
     */
    public UsersInfo getUsersInfo() {

        String sqlQuery = "{CALL get_users_info()}";
        try {
            UsersInfo usersInfo = jdbcTemplate.queryForObject(sqlQuery, new RowMapper<UsersInfo>(){
                @Override
                public UsersInfo mapRow(ResultSet rs, int rownumber) throws SQLException {
                    return new UsersInfo(
                            rs.getInt("utilisateurs"),
                            rs.getInt("actifs"),
                            rs.getInt("inactifs"),
                            rs.getInt("super_administrateurs"),
                            rs.getInt("administrateurs"),
                            rs.getInt("moderateurs"),
                            rs.getInt("membres")
                    );
                }
            });
            if (usersInfo != null) {
                logger.info(usersInfo.toString());
            }
            return usersInfo;
        } catch (Exception e) {
            logger.error("Erreur: " + e.getMessage());
            return null;
        }
    }

}
