package com.ginius.jbosse.dao;

import com.ginius.jbosse.models.anecdote.Anecdote;
import com.ginius.jbosse.models.comment.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface CommentRepository extends JpaRepository<Comment, Long> {

	/**
	 * Méthode qui retourne des commentaires via une pagination
	 * @return list
	 */
	@Query(value ="select x from Comment x ")
	List<Comment> getComments(Pageable paging);

	List<Comment> getAllByAnecdote(Anecdote a);
	
	/**
	 * Méthode qui retourne une page de commentaires d'un anecdote
	 * @param anecdoteId id de l'anecdote
	 * @param pageable configuration de la pagination
	 * @return une pagination
	 */
	@Query("select x from Comment x where x.anecdote.id = :num")
	public Page<Comment> commentsList(@Param("num") int anecdoteId, Pageable pageable);

}
