package com.ginius.jbosse;

import com.ginius.jbosse.dao.*;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.Role;
import com.ginius.jbosse.models.Sexe;
import com.ginius.jbosse.models.anecdote.Anecdote;
import com.ginius.jbosse.models.anecdote.AnecdoteCategory;
import com.ginius.jbosse.models.anecdote.status.AnecdoteStatus;
import com.ginius.jbosse.models.anecdote.status.NotationAnecdote;
import com.ginius.jbosse.models.comment.Comment;
import com.ginius.jbosse.models.comment.status.CommentStatus;
import com.ginius.jbosse.models.comment.status.NotationComment;
import com.ginius.jbosse.utils.exception.RoleNotFoundException;
import com.ginius.jbosse.utils.UtilsApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class JbosseApplication implements CommandLineRunner {

    @Bean
    public BCryptPasswordEncoder getBCPE() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private RepositoryRestConfiguration repositoryRestConfiguration;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    AnecdoteCategoryRepository anecdoteCategoryRepository;
    @Autowired
    AnecdoteRepository anecdoteRepository;
    @Autowired
    NotationAnecdoteRepository notationAnecdoteRepository;
    @Autowired
    AnecdoteStatusRepository anecdoteStatusRepository;
    @Autowired
    CommentStatusRepository commentStatusRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    NotationCommentRepository notationCommentRepository;

    public static void main(String[] args) {
        SpringApplication.run(JbosseApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        repositoryRestConfiguration.exposeIdsFor(Member.class, Role.class);
        List<String> alUserRoles = UtilsApi.generationList("D:\\git\\repository\\jbosse\\jbosse-back_end\\documents\\roles.txt");
        List<String> categories = UtilsApi.generationList("D:\\git\\repository\\jbosse\\jbosse-back_end\\documents\\anecdoteActegories.txt");
        List<String> anecdoteStatus = UtilsApi.generationList("D:\\git\\repository\\jbosse\\jbosse-back_end\\documents\\statusAnecdote.txt");
        List<String> CommentStatus = UtilsApi.generationList("D:\\git\\repository\\jbosse\\jbosse-back_end\\documents\\statusComment.txt");

        for (String s : alUserRoles
        ) {
            roleRepository.save(new Role(null, s, 0, 0));
        }

        Role superAdmin = roleRepository.findRoleByLibelle(("SUPER_ADMIN").toUpperCase())
                .orElseThrow(() -> new RoleNotFoundException("SUPER_ADMIN"));
        Role admin = roleRepository.findRoleByLibelle(("ADMIN").toUpperCase())
                .orElseThrow(() -> new RoleNotFoundException("ADMIN"));

        Role mod = roleRepository.findRoleByLibelle(("MOD").toUpperCase())
                .orElseThrow(() -> new RoleNotFoundException("MOD"));
        Role user = roleRepository.findRoleByLibelle(("USER").toUpperCase())
                .orElseThrow(() -> new RoleNotFoundException("USER"));

        Collection<Role> rolesSuper = new ArrayList<>();
        rolesSuper.add(superAdmin);
        rolesSuper = UtilsApi.generationUserRoles(rolesSuper, roleRepository.findAll());

        Collection<Role> rolesAdm = new ArrayList<>();
        rolesAdm.add(admin);
        rolesAdm = UtilsApi.generationUserRoles(rolesAdm, roleRepository.findAll());

        Collection<Role> rolesMod = new ArrayList<>();
        rolesMod.add(mod);
        rolesMod = UtilsApi.generationUserRoles(rolesMod, roleRepository.findAll());

        Collection<Role> rolesUsr = new ArrayList<>();
        rolesUsr.add(user);
        rolesUsr = UtilsApi.generationUserRoles(rolesUsr, roleRepository.findAll());

       Member user1 = memberRepository.save(new Member("ginius4@hotmail.com", "ginius", getBCPE().encode("1234"), "derieu", "vincent",
                true, "ginius.jpg", Sexe.HOMME, rolesSuper));
        Member user2 = memberRepository.save(new Member("bouh20@hotmail.com", "bouh20", getBCPE().encode("1234"), "derieu", "lise",
                true, "bouh20.jpg", Sexe.FEMME, rolesAdm));
        Member user3 = memberRepository.save(new Member("dominique.derieu@outlook.com", "dom", getBCPE().encode("1234"), "derieu", "dominique",
                true, "dom.jpg", Sexe.HOMME, rolesMod));
        Member user4 = memberRepository.save(new Member("maelisd@gmail.com", "captainChi", getBCPE().encode("1234"), "derieu", "maêlis",
                true, "captainChi.jpg", Sexe.FEMME, rolesUsr));


        for (String s : categories
        ) {
            anecdoteCategoryRepository.save(new AnecdoteCategory(s, s + ".jpg",s + "-back.jpg"));
        }

        Anecdote a1 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, " +
                "sed quia nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user1));
        Anecdote a2 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua" +
                " vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("santé"), user1));
        Anecdote a3 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia " +
                "nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user2));
        Anecdote a4 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque " +
                "praecipua vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("restauration"), user2));
        Anecdote a5 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia " +
                "nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user3));
        Anecdote a6 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque " +
                "praecipua vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("éducation"), user3));
        Anecdote a7 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc " +
                "sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("sport"), user3));
        Anecdote a8 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque " +
                "praecipua vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("santé"), user3));
        Anecdote a9 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, sed quia nunc " +
                "sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("tourisme"), user3));
        Anecdote a10 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua " +
                "vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("tourisme"), user3));
        Anecdote a11 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua " +
                "vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("tourisme"), user2));
        Anecdote a12 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua " +
                "vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("sport"), user1));
        Anecdote a13 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Ex turba vero imae sortis et paupertinae in tabernis aliqui pernoctant vinariis, non nulli velariis umbraculorum theatralium latent, quae Campanam imitatus lasciviam Catulus in aedilitate sua suspendit omnium primus; aut pugnaciter aleis certant turpi sono fragosis naribus introrsum reducto spiritu concrepantes; aut quod est studiorum omnium maximum ab ortu lucis ad vesperam sole fatiscunt vel pluviis, per minutias aurigarum equorumque praecipua " +
                "vel delicta scrutantes.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("éducation"), user2));
        Anecdote a14 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, " +
                "sed quia nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user1));
        Anecdote a15 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, " +
                "sed quia nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user1));
        Anecdote a16 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, " +
                "sed quia nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user1));
        Anecdote a17 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, " +
                "sed quia nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user1));
        Anecdote a18 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, " +
                "sed quia nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user1));
        Anecdote a19 = anecdoteRepository.save(new Anecdote(LocalDateTime.now(), "Omitto iuris dictionem in libera civitate contra leges senatusque consulta; caedes relinquo; libidines praetereo, quarum acerbissimum extat indicium et ad insignem memoriam turpitudinis et paene ad iustum odium imperii nostri, quod constat nobilissimas virgines se in puteos abiecisse et morte voluntaria necessariam turpitudinem depulisse. Nec haec idcirco omitto, quod non gravissima sint, " +
                "sed quia nunc sine teste dico.", anecdoteCategoryRepository.findAnecdoteCategoryByLabel("artisanat"), user1));


        for (String s : anecdoteStatus
        ) {
            anecdoteStatusRepository.save(new AnecdoteStatus(s));
        }

        Optional<AnecdoteStatus> fun = anecdoteStatusRepository.getAnecdoteStatusByLabel("FUN");
        Optional<AnecdoteStatus> des = anecdoteStatusRepository.getAnecdoteStatusByLabel("DES");
        Optional<AnecdoteStatus> sad = anecdoteStatusRepository.getAnecdoteStatusByLabel("SAD");


        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a1.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a2.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user2.getEmail(), fun.get().getId(), a2.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a3.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user2.getEmail(), fun.get().getId(), a3.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a4.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a5.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user2.getEmail(), fun.get().getId(), a6.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a7.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user2.getEmail(), fun.get().getId(), a8.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user2.getEmail(), fun.get().getId(), a9.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user2.getEmail(), fun.get().getId(), a10.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a11.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a12.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user2.getEmail(), fun.get().getId(), a13.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a9.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), fun.get().getId(), a13.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user3.getEmail(), fun.get().getId(), a13.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), des.get().getId(), a1.getId()));
        notationAnecdoteRepository.save(new NotationAnecdote(user1.getEmail(), sad.get().getId(), a2.getId()));

        for (String s : CommentStatus
        ) {
            commentStatusRepository.save(new CommentStatus(s));
        }

        Optional<CommentStatus> userfull = commentStatusRepository.getCommentStatusByLabel("USEFULL");
        Optional<CommentStatus> userless = commentStatusRepository.getCommentStatusByLabel("USELESS");


		Comment c1 = commentRepository.save(new Comment("premier commentaire anecdote 1", LocalDateTime.now(), a1, user2));
		Comment c2 = commentRepository.save(new Comment("deuxième commentaire anecdote 1",  LocalDateTime.now(), a1, user3));
		Comment c3 = commentRepository.save(new Comment("premier commentaire anecdote 2",  LocalDateTime.now(), a2, user2));
		Comment c4 = commentRepository.save(new Comment("premier commentaire anecdote 3",  LocalDateTime.now(), a3, user3));
		Comment c5 = commentRepository.save(new Comment("premier commentaire anecdote 4",  LocalDateTime.now(), a4, user3));
		Comment c6 = commentRepository.save(new Comment("premier commentaire anecdote 5",  LocalDateTime.now(), a5, user2));
		Comment c7 = commentRepository.save(new Comment("premier commentaire anecdote 6",  LocalDateTime.now(), a6, user1));
		Comment c8 = commentRepository.save(new Comment("deuxième commentaire anecdote 6",  LocalDateTime.now(), a6, user2));
        Comment c9 = commentRepository.save(new Comment("deuxième commentaire anecdote 1",  LocalDateTime.now(), a1, user1));



        notationCommentRepository.save(new NotationComment(user1.getEmail(), userfull.get().getId(), c1.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userfull.get().getId(), c2.getId()));
        notationCommentRepository.save(new NotationComment(user2.getEmail(), userfull.get().getId(), c2.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userfull.get().getId(), c3.getId()));
        notationCommentRepository.save(new NotationComment(user2.getEmail(), userfull.get().getId(), c3.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userfull.get().getId(), c4.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userfull.get().getId(), c5.getId()));
        notationCommentRepository.save(new NotationComment(user2.getEmail(), userfull.get().getId(), c6.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userfull.get().getId(), c7.getId()));
        notationCommentRepository.save(new NotationComment(user2.getEmail(), userfull.get().getId(), c8.getId()));
        notationCommentRepository.save(new NotationComment(user3.getEmail(), userfull.get().getId(), c8.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userless.get().getId(), c5.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userless.get().getId(), c5.getId()));
        notationCommentRepository.save(new NotationComment(user2.getEmail(), userless.get().getId(), c6.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userless.get().getId(), a9.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userless.get().getId(), c6.getId()));
        notationCommentRepository.save(new NotationComment(user3.getEmail(), userless.get().getId(), c3.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userless.get().getId(), c3.getId()));
        notationCommentRepository.save(new NotationComment(user1.getEmail(), userless.get().getId(), c3.getId()));

    }
}
