package com.ginius.jbosse.securite;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class JWTAutorisationFilter extends OncePerRequestFilter {

    private final Logger logger = Logger.getLogger(JWTAutorisationFilter.class);
    private String logRoles = "";

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        logger.debug("[doFilterInternal] - Appel de la méthode");

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Headers",
                "Origin, Accept, X-Requested-With, Content-Type, Content-Length," +
                        " Access-Control-Request-Method" +
                        ", Access-Control-Request-Headers, Authorization");
        response.addHeader("Access-Control-Expose-Headers",
                "Access-Control-Allow-Origin, " +
                        "Access-Control-Allow-Credentials, Authorization, Content-Length");
        response.addHeader("access-control-allow-methods",
                "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS");

        String jwt = request.getHeader(SecurityConstants.HEADER_STRING);

        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            if (jwt == null || !jwt.startsWith(SecurityConstants.TOKEN_PREFIX)) {
                filterChain.doFilter(request, response);
                logger.debug("[doFilterInternal] - JWT inconnu - fin de l'appel de la méthode");
                return;
            }

            Claims claims = Jwts.parser()
                    .setSigningKey(SecurityConstants.SECRET)
                    .parseClaimsJws(jwt.replace(SecurityConstants.TOKEN_PREFIX, ""))
                    .getBody();

            String username = claims.getSubject();
            ArrayList<Map<String, String>> roles = (ArrayList<Map<String, String>>) claims.get("roles");
            Collection<GrantedAuthority> autorities = new ArrayList<>();

            logRoles = ""; //initialisation de la liste des roles

            roles.forEach(r -> {
                autorities.add(new SimpleGrantedAuthority(r.get("authority")));
                logRoles += r.get("authority") + " ";
            });

            logger.debug("[doFilterInternal] - Utilisateur " + username + " Authentifié avec les rôles : " + logRoles);

            UsernamePasswordAuthenticationToken autenticatedUser =
                    new UsernamePasswordAuthenticationToken(username, null, autorities);
            SecurityContextHolder.getContext().setAuthentication(autenticatedUser);
            filterChain.doFilter(request, response);

            logger.debug("[doFilterInternal] - Fin de l'appel de la méthode");
        }
    }
}
