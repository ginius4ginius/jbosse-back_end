package com.ginius.jbosse.securite.web;

import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.Role;
import com.ginius.jbosse.securite.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/account")
public class AccountRestController {

    @Autowired
    AccountService accountService;

    /**
     * Méthode permettant de sauvegarder un utilisateur
     *
     * @param member Member
     * @return User
     */
    @PostMapping("/saveUser")
    public Member sauvegarde(@RequestBody Member member) {

        return accountService.saveUser(member);

    }

    /**
     * Méthode permettant de sauvegarder un role
     *
     * @param role role
     * @return Role
     */
    @PostMapping("/saveRole")
    public Role sauvegarde(@RequestBody Role role) {

        return accountService.saveRole(role);

    }

    /**
     * Méthode permettant d'ajouter un role à un utilisateur
     * @param username username de l'utilisateur
     * @param libelle libelle du role
     * @return ResponseEntoty //200 ou //404
     */
    @PostMapping("/addRole")
    public ResponseEntity addRoleToUser(@Param("username") String username,
                                        @Param("libelle") String libelle) {
        return accountService.addRoleToUser(username, libelle);
    }

}
