package com.ginius.jbosse.securite;

public class SecurityConstants {

    protected static final long EXERATION_DATE = 864_000_000 ; //10 jours
    protected static final String TOKEN_PREFIX = "Bearer ";
    protected static final String HEADER_STRING = "Authorization" ;
    protected static final String SECRET = "ginius4ginius";

}
