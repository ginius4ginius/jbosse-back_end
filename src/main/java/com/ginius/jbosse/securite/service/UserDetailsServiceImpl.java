package com.ginius.jbosse.securite.service;

import com.ginius.jbosse.models.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    AccountService accountService;

    /**
     * Méthode générant un utilisateur UserDetails avec sa liste de roles ( GrantedAuthority )
     * @param username username de l'utilisateuer
     * @return UserDetails
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Collection<GrantedAuthority> autorities = new ArrayList<>();
        Member member  =accountService.findUserByUsername(username);

        if(member==null){
            throw new UsernameNotFoundException(username);
        }

        member.getRoles().forEach(r ->{
            autorities.add(new SimpleGrantedAuthority(r.getLibelle()));
        });
        return new User(member.getUsername(), member.getPassword(),autorities );
    }
}

