package com.ginius.jbosse.securite.service;

import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.Role;
import org.springframework.http.ResponseEntity;

public interface AccountService {

    public Member saveUser(Member user);
    public Role saveRole(Role role);
    public ResponseEntity addRoleToUser(String username, String roleLibelle);
    public Member findUserByUsername(String username);

}
