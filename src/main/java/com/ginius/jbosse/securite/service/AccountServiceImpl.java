package com.ginius.jbosse.securite.service;

import com.ginius.jbosse.dao.RoleRepository;
import com.ginius.jbosse.dao.MemberRepository;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.Role;
import com.ginius.jbosse.utils.exception.RoleNotFoundException;
import com.ginius.jbosse.utils.exception.UserNotFoundException;
import com.ginius.jbosse.web.RoleRestController;
import com.ginius.jbosse.web.UserRestController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final Logger logger = Logger.getLogger(AccountServiceImpl.class);


    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    @Autowired
    private UserRestController userRestController;
    @Autowired
    private RoleRestController roleRestController;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private MemberRepository memberRepository;

    /**
     * Méthode permettant de sauvegarder un utilisateur
     *
     * @param member Member
     * @return HrUser
     */
    @Override
    public Member saveUser(Member member) {
        member.setPassword(bCryptPasswordEncoder.encode(member.getPassword()));
        return (Member) userRestController.saveUser(member).getBody();
    }

    /**
     * Méthode permettant de sauvegarder un role
     *
     * @param role HrRole
     * @return HrRole
     */
    @Override
    public Role saveRole(Role role) {
        return roleRestController.saveRole(role);
    }

    /**
     * Méthode permettantde rajouter un role à un utilisateur
     *
     * @param username    username de l'utilisateur
     * @param roleLibelle le nom du role
     */
    @Override
    public ResponseEntity<String> addRoleToUser(String username, String roleLibelle) {
        logger.warn("Tentative de récupération de l'utilisateur et du role");
        Member member = memberRepository.findUserByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
        Role role = roleRepository.findRoleByLibelle(roleLibelle).orElseThrow(() -> new RoleNotFoundException(roleLibelle));

        member.getRoles().add(role);
        logger.warn("Ajout du rôle à la liste des roles de l'utilisateur réussi.");
        return new ResponseEntity<>("Mise à jour effectuée", HttpStatus.OK); // 200
    }

    /**
     * Méthode permettant de trouver un utilisateur par son username.
     *
     * @param username le login de l'utilisateur
     * @return un Member
     */
    @Override
    public Member findUserByUsername(String username) {
        return memberRepository.findUserByUsername(username).orElseThrow(()->new UserNotFoundException(username));
    }
}
