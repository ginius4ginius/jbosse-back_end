package com.ginius.jbosse.securite;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginius.jbosse.models.Member;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class JWTAuthentificationFilter extends UsernamePasswordAuthenticationFilter {

    private final Logger logger = Logger.getLogger(JWTAuthentificationFilter.class);

    private AuthenticationManager authenticationManager;

    public JWTAuthentificationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        logger.debug("[attemptAuthentication] - Appel de la méthode");
        Member member = null;

        try {
            member = new ObjectMapper().readValue(request.getInputStream(), Member.class);
        } catch (IOException e) {
            logger.warn("Erreur Lors du mapping de l'utilisateur " + e.getMessage());
            throw new RuntimeException(e);
        }
        logger.debug("[attemptAuthentication] - Fin de l'appel de la méthode");

        return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(member.getUsername(), member.getPassword()));


    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response, FilterChain filterChain,
                                            Authentication authResult) throws IOException, ServletException {

        logger.debug("[successfulAuthentication] - Appel de la méthode");

        User user = (User) authResult.getPrincipal();
        logger.info("Utilisateur " + user.getUsername() + " authentifié avec succés");
        String jwtToken = Jwts.builder().setSubject(user.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXERATION_DATE))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET)
                .claim("roles", user.getAuthorities())
                .compact();

        response.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + jwtToken);

        logger.debug("[successfulAuthentication] - Fin de l'appel de la méthode");

    }

}
