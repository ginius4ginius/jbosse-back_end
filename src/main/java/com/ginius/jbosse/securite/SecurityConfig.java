package com.ginius.jbosse.securite;

import com.ginius.jbosse.securite.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userDetailsServiceImpl)
                .passwordEncoder(bCryptPasswordEncoder);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        //http.formLogin();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //POST ALL
        http.authorizeRequests().antMatchers(HttpMethod.POST,
                "/account/saveUser/**","/account/user").permitAll();

        //POST ADMIN
        http.authorizeRequests().antMatchers(HttpMethod.POST,"/account/**")
                .hasAnyAuthority("ADMIN", "SUPER_ADMIN");
        //POST ALL
        http.authorizeRequests().antMatchers(HttpMethod.POST,
                "/api/**")
                .hasAnyAuthority("USER", "MOD","ADMIN", "SUPER_ADMIN");
        //GET ADMIN
        http.authorizeRequests().antMatchers(HttpMethod.GET,
                "/users/**", "/roles/**", "/account/**")
                .hasAnyAuthority("ADMIN", "SUPER_ADMIN");
        //PUT ADMIN
        http.authorizeRequests().antMatchers(HttpMethod.PUT,
                "/users/**", "/roles/**", "/account/**")
                .hasAnyAuthority("ADMIN", "SUPER_ADMIN");
        //ALL
        http.authorizeRequests().antMatchers(HttpMethod.GET,
                "/account/**")
                .hasAnyAuthority("USER", "MOD", "ADMIN", "SUPER_ADMIN");
        http.authorizeRequests().antMatchers("/login/**", "/photoUser/**","/api/photoAnecdoteCategory/**",
                "/api/photoAnecdoteCategoryBack/**").permitAll();
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(new JWTAuthentificationFilter(authenticationManager()));
        http.addFilterBefore(new JWTAutorisationFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}

