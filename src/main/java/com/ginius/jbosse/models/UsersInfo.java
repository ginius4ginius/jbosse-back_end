package com.ginius.jbosse.models;

public class UsersInfo {
    private int utilisateurs;
    private int actifs;
    private int inactifs;
    private int superAdministrateurs;
    private int administrateurs;
    private int moderateurs;
    private int membres;

    public UsersInfo(int utilisateurs, int actifs, int inactifs, int superAdministrateurs, int administrateurs, int moderateurs, int membres) {
        this.utilisateurs = utilisateurs;
        this.actifs = actifs;
        this.inactifs = inactifs;
        this.superAdministrateurs = superAdministrateurs;
        this.administrateurs = administrateurs;
        this.moderateurs = moderateurs;
        this.membres = membres;
    }

    public int getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(int utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    public int getActifs() {
        return actifs;
    }

    public void setActifs(int actifs) {
        this.actifs = actifs;
    }

    public int getInactifs() {
        return inactifs;
    }

    public void setInactifs(int inactifs) {
        this.inactifs = inactifs;
    }

    public int getSuperAdministrateurs() {
        return superAdministrateurs;
    }

    public void setSuperAdministrateurs(int superAdministrateurs) {
        this.superAdministrateurs = superAdministrateurs;
    }

    public int getAdministrateurs() {
        return administrateurs;
    }

    public void setAdministrateurs(int administrateurs) {
        this.administrateurs = administrateurs;
    }

    public int getModerateurs() {
        return moderateurs;
    }

    public void setModerateurs(int moderateurs) {
        this.moderateurs = moderateurs;
    }

    public int getMembres() {
        return membres;
    }

    public void setMembres(int membres) {
        this.membres = membres;
    }

    @Override
    public String toString() {
        return "UsersInfo{" +
                "utilisateurs=" + utilisateurs +
                ", actifs=" + actifs +
                ", inactifs=" + inactifs +
                ", superAdministrateurs=" + superAdministrateurs +
                ", administrateurs=" + administrateurs +
                ", moderateurs=" + moderateurs +
                ", membres=" + membres +
                '}';
    }

}
