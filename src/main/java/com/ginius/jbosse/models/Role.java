package com.ginius.jbosse.models;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Role implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    @Size(min = 2, max = 50)
    private String libelle;
    @Version
    private int uVersion;
    private int gVersion;

    public Role() {
    }

    public Role(Long id, String libelle, int uversion, int gversion) {
        this.id = id;
        this.libelle = libelle;
        this.uVersion = uversion;
        this.gVersion = gversion;
    }


    public Long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getuVersion() {
        return uVersion;
    }

    public void setuVersion(int uVersion) {
        this.uVersion = uVersion;
    }

    public int getgVersion() {
        return gVersion;
    }

    public void setgVersion(int gVersion) {
        this.gVersion = gVersion;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", uVersion=" + uVersion +
                ", gVersion=" + gVersion +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return uVersion == role.uVersion &&
                gVersion == role.gVersion &&
                Objects.equals(id, role.id) &&
                Objects.equals(libelle, role.libelle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, libelle, uVersion, gVersion);
    }
}
