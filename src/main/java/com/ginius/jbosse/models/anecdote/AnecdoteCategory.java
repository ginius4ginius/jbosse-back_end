package com.ginius.jbosse.models.anecdote;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Classe représentant la table anecdote_category de la SGBDR
 * 
 * @author Derieu
 *
 */
@Entity
@Table(name = "anecdote_category")
public class AnecdoteCategory implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id")
	private long id;
	
	@NotNull
	@Column(name = "label")
	private String label;

	private String photo;

	private String photoBack;
	
	//constructeur
	
	public AnecdoteCategory() {
		
	}
	
	public AnecdoteCategory(String label, String photo, String photoBack) {
		this.label = label;
		this.photo = photo;
		this.photoBack = photoBack;
	}
	
	//getters et setters


	public String getPhotoBack() {
		return photoBack;
	}

	public void setPhotoBack(String photoBack) {
		this.photoBack = photoBack;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AnecdoteCategory that = (AnecdoteCategory) o;
		return id == that.id &&
				Objects.equals(label, that.label) &&
				Objects.equals(photo, that.photo) &&
				Objects.equals(photoBack, that.photoBack);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, label, photo, photoBack);
	}

	@Override
	public String toString() {
		return "AnecdoteCategory{" +
				"id=" + id +
				", label='" + label + '\'' +
				", photo='" + photo + '\'' +
				", photoBack='" + photoBack + '\'' +
				'}';
	}
}
