package com.ginius.jbosse.models.anecdote;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.comment.Comment;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Classe représentant la table anecdote de la SGCDR
 *
 * @author Derieu
 */
@Entity
@Table(name = "anecdote")
public class Anecdote implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(name = "published_at")
    private LocalDateTime publishedAt;

    @Column(name = "active", columnDefinition = "BOOLEAN DEFAULT TRUE")
    private boolean active = true;

    @NotNull
    @Column(name = "description", length = 500)
    private String description;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "category_id", foreignKey = @ForeignKey(name = "fk_anecdote_anecdote_category"))
    private AnecdoteCategory category;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_email", foreignKey = @ForeignKey(name = "fk_anecdote_user"))
    private Member user;

    public Anecdote() {
    }

    public Anecdote(LocalDateTime publishedAt, String description, AnecdoteCategory category, Member user) {
        this.publishedAt = publishedAt;
        this.description = description;
        this.category = category;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(LocalDateTime publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AnecdoteCategory getCategory() {
        return category;
    }

    public void setCategory(AnecdoteCategory category) {
        this.category = category;
    }

    public Member getUser() {
        return user;
    }

    public void setUser(Member user) {
        this.user = user;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isactive() {
        return active;
    }

    public void setactive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Anecdote anecdote = (Anecdote) o;
        return id == anecdote.id &&
                active == anecdote.active &&
                Objects.equals(publishedAt, anecdote.publishedAt) &&
                Objects.equals(description, anecdote.description) &&
                Objects.equals(category, anecdote.category) &&
                Objects.equals(user, anecdote.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, publishedAt, active, description, category, user);
    }

    @Override
    public String toString() {
        return "Anecdote{" +
                "id=" + id +
                ", publishedAt=" + publishedAt +
                ", active=" + active +
                ", description='" + description + '\'' +
                ", category=" + category +
                ", user=" + user +
                '}';
    }
}
