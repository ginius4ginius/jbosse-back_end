package com.ginius.jbosse.models.anecdote.status;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Classe  représentant la table anecdote_status de la SGBDR
 *
 * @author Derieu
 */
@Entity
@Table(name = "anecdote_status")
public class AnecdoteStatus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 3, max = 3)
    private String label;

    public AnecdoteStatus() {

    }

    /**
     * @param label du status
     */
    public AnecdoteStatus(String label) {
        this.label = label;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnecdoteStatus that = (AnecdoteStatus) o;
        return id == that.id &&
                Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label);
    }

    @Override
    public String toString() {
        return "AnecdoteStatus{" +
                "id=" + id +
                ", label='" + label + '\'' +
                '}';
    }
}
