package com.ginius.jbosse.models.anecdote.status;

import javax.persistence.*;

@Entity
@Table(name = "notation_anecdote")
@IdClass(NotationAnecdotePK.class)
public class NotationAnecdote {

    @Id
    @Column(name = "member_email")
    private String userEmail;

    @Id
    @Column(name = "status_id")
    private long statusId;

    @Id
    @Column(name = "anecdote_id")
    private long anecdoteId;

    public NotationAnecdote() {
    }

    public NotationAnecdote(String userEmail, long statusId, long anecdoteId) {
        this.userEmail = userEmail;
        this.statusId = statusId;
        this.anecdoteId = anecdoteId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public long getAnecdoteId() {
        return anecdoteId;
    }

    public void setAnecdoteId(long anecdoteId) {
        this.anecdoteId = anecdoteId;
    }
}
