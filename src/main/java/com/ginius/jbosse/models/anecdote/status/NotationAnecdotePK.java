package com.ginius.jbosse.models.anecdote.status;

import java.io.Serializable;

public class NotationAnecdotePK implements Serializable {

    private String userEmail;
    private long statusId;
    private long anecdoteId;

    public String getUserId() {
        return userEmail;
    }

    public void setUserId(String userEmail) {
        this.userEmail = userEmail;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public long getAnecdoteId() {
        return anecdoteId;
    }

    public void setAnecdoteId(long anecdoteId) {
        this.anecdoteId = anecdoteId;
    }

    public NotationAnecdotePK(){}

    public NotationAnecdotePK(String userEmail, long statusId, long anecdoteId){
        this.userEmail = userEmail;
        this.statusId = statusId;
        this.anecdoteId = anecdoteId;
    }


}
