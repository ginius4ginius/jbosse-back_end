package com.ginius.jbosse.models.anecdote;

import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.anecdote.status.NotationAnecdote;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class AnecdoteDto implements Serializable {

    private long id;

    private LocalDateTime publishedAt;

    private String description;

    private AnecdoteCategory category;

    private Member user;

    private List<List<NotationAnecdote>> votes;

    private boolean isVisible;

    private boolean active;

    public AnecdoteDto(long id, LocalDateTime publishedAt, String description, AnecdoteCategory category, Member user,
                       List<List<NotationAnecdote>> votes, boolean active) {
        this.id = id;
        this.publishedAt = publishedAt;
        this.description = description;
        this.category = category;
        this.user = user;
        this.votes = votes;
        this.active = active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(LocalDateTime publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AnecdoteCategory getCategory() {
        return category;
    }

    public void setCategory(AnecdoteCategory category) {
        this.category = category;
    }

    public Member getUser() {
        return user;
    }

    public void setUser(Member user) {
        this.user = user;
    }

    public List<List<NotationAnecdote>> getVotes() {
        return votes;
    }

    public void setVotes(List<List<NotationAnecdote>> votes) {
        this.votes = votes;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnecdoteDto that = (AnecdoteDto) o;
        return id == that.id &&
                isVisible == that.isVisible &&
                active == that.active &&
                Objects.equals(publishedAt, that.publishedAt) &&
                Objects.equals(description, that.description) &&
                Objects.equals(category, that.category) &&
                Objects.equals(user, that.user) &&
                Objects.equals(votes, that.votes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, publishedAt, description, category, user, votes, isVisible, active);
    }

    @Override
    public String toString() {
        return "AnecdoteDto{" +
                "id=" + id +
                ", publishedAt=" + publishedAt +
                ", description='" + description + '\'' +
                ", category=" + category +
                ", user=" + user +
                ", votes=" + votes +
                ", isVisible=" + isVisible +
                ", active=" + active +
                '}';
    }

}
