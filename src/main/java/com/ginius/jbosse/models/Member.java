package com.ginius.jbosse.models;

import com.ginius.jbosse.utils.validation.annotation.ValidEmail;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

@Entity
public class Member implements Serializable {

    @Id
    @ValidEmail
    @Column(unique=true, nullable = false, length = 50)
    private String email;

    @Column(unique=true, nullable = false)
    @Size(min = 3, max = 10)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    @Size(min = 2, max = 20)
    private String nom;

    @Column(nullable = false)
    @Size(min = 2, max = 20)
    private String prenom;

    @Column(length = 5)
    @Enumerated(EnumType.STRING)
    private Sexe sexe;

    @Column(columnDefinition = "boolean default false")
    private Boolean actif;

    @Column(nullable = false)
    private LocalDateTime dateCreation;
    private String photo;

    @Version
    private int uVersion;
    private int gVersion;

    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles = new ArrayList<>();


    public Member() {
    }

    public Member(String email, String username, String password, String nom, String prenom, Boolean actif, String photo,
                  Sexe sexe, Collection<Role> roles) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.actif = actif;
        this.dateCreation = LocalDateTime.now();
        this.roles = roles;
        this.photo = photo;
        this.sexe = sexe;
        this.gVersion = 0;
        this.uVersion = 0;
    }

    public String getEmail() {
        return email;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //@JsonIgnore
    public String getPassword() {
        return password;
    }

    //@JsonSetter
    public void setPassword(String password) {
        this.password = password;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getuVersion() {
        return uVersion;
    }

    public void setuVersion(int uVersion) {
        this.uVersion = uVersion;
    }

    public int getgVersion() {
        return gVersion;
    }

    public void setgVersion(int gVersion) {
        this.gVersion = gVersion;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    @Override
    public String toString() {
        return "Member{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", sexe=" + sexe +
                ", actif=" + actif +
                ", dateCreation=" + dateCreation +
                ", photo='" + photo + '\'' +
                ", uVersion=" + uVersion +
                ", gVersion=" + gVersion +
                ", roles=" + roles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return uVersion == member.uVersion &&
                gVersion == member.gVersion &&
                Objects.equals(email, member.email) &&
                Objects.equals(username, member.username) &&
                Objects.equals(password, member.password) &&
                Objects.equals(nom, member.nom) &&
                Objects.equals(prenom, member.prenom) &&
                sexe == member.sexe &&
                Objects.equals(actif, member.actif) &&
                Objects.equals(dateCreation, member.dateCreation) &&
                Objects.equals(photo, member.photo) &&
                Objects.equals(roles, member.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, username, password, nom, prenom, sexe, actif, dateCreation, photo, uVersion, gVersion, roles);
    }
}
