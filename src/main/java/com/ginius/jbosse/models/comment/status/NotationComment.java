package com.ginius.jbosse.models.comment.status;

import javax.persistence.*;

@Entity
@Table(name = "notation_comment")
@IdClass(NotationCommentPK.class)
public class NotationComment {

    @Id
    @Column(name = "member_email")
    private String userEmail;

    @Id
    @Column(name = "status_id")
    private long statusId;

    @Id
    @Column(name = "comment_id")
    private long commentId;

    public NotationComment() {
    }

    public NotationComment(String userEmail, long statusId, long commentId) {
        this.userEmail = userEmail;
        this.statusId = statusId;
        this.commentId = commentId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long anecdoteId) {
        this.commentId = commentId;
    }
}
