package com.ginius.jbosse.models.comment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.anecdote.Anecdote;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Classe  représentant la table comment de la SGBDR
 * 
 * @author Derieu
 *
 */
@Entity
@Table(name = "comment")
public class Comment implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_id")
	private long id;

	@NotNull
	@Column(name = "description")
	private String description;

	@NotNull
	@Column(name = "published_at")
	private LocalDateTime publishedAt;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "anecdote_id", foreignKey = @ForeignKey(name = "fk_comment_anecdote"))
	private Anecdote anecdote;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "email", foreignKey = @ForeignKey(name = "fk_comment_user"))
	private Member user;

	// constructeur

	public Comment() {

	}

	public Comment(String description, LocalDateTime publishedAt, Anecdote anecdote, Member user) {
		super();
		this.description = description;
		this.publishedAt = publishedAt;
		this.anecdote = anecdote;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(LocalDateTime publishedAt) {
		this.publishedAt = publishedAt;
	}

	public Anecdote getAnecdote() {
		return anecdote;
	}

	public void setAnecdote(Anecdote anecdote) {
		this.anecdote = anecdote;
	}

	public Member getUser() {
		return user;
	}

	public void setUser(Member user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, description, publishedAt, anecdote, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (anecdote == null) {
			if (other.anecdote != null)
				return false;
		} else if (!anecdote.equals(other.anecdote))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (publishedAt == null) {
			if (other.publishedAt != null)
				return false;
		} else if (!publishedAt.equals(other.publishedAt))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", description=" + description + ", publishedAt=" + publishedAt + ", anecdote="
				+ anecdote + ", user=" + user + "]";
	}

}
