package com.ginius.jbosse.models.comment;

import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.anecdote.Anecdote;
import com.ginius.jbosse.models.comment.status.NotationComment;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class CommentDto implements Serializable {

    private long id;
    private String description;
    private LocalDateTime publishedAt;
    private Anecdote anecdote;
    private Member user;
    private List<List<NotationComment>> votes;
    private boolean isVisible;

    public CommentDto(long id, String description, LocalDateTime publishedAt, Anecdote anecdote,
                      Member user, List<List<NotationComment>> votes) {
        this.id = id;
        this.description = description;
        this.publishedAt = publishedAt;
        this.anecdote = anecdote;
        this.user = user;
        this.votes = votes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(LocalDateTime publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Anecdote getAnecdote() {
        return anecdote;
    }

    public void setAnecdote(Anecdote anecdote) {
        this.anecdote = anecdote;
    }

    public Member getUser() {
        return user;
    }

    public void setUser(Member user) {
        this.user = user;
    }

    public List<List<NotationComment>> getVotes() {
        return votes;
    }

    public void setVotes(List<List<NotationComment>> votes) {
        this.votes = votes;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentDto that = (CommentDto) o;
        return id == that.id &&
                isVisible == that.isVisible &&
                Objects.equals(description, that.description) &&
                Objects.equals(publishedAt, that.publishedAt) &&
                Objects.equals(anecdote, that.anecdote) &&
                Objects.equals(user, that.user) &&
                Objects.equals(votes, that.votes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, publishedAt, anecdote, user, votes, isVisible);
    }

    @Override
    public String toString() {
        return "CommentDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", publishedAt=" + publishedAt +
                ", anecdote=" + anecdote +
                ", user=" + user +
                ", votes=" + votes +
                ", isVisible=" + isVisible +
                '}';
    }
}
