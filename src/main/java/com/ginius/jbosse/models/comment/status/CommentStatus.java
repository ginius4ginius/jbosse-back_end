package com.ginius.jbosse.models.comment.status;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * Classe représentant la table comment_status de la SGBDR
 *
 * @author Derieu
 */
@Entity
@Table(name = "comment_status")
public class CommentStatus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min = 7, max = 7)
    private String label;

    public CommentStatus() {
    }

    /**
     * @param label du status
     */
    public CommentStatus(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentStatus that = (CommentStatus) o;
        return id == that.id &&
                Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label);
    }

    @Override
    public String toString() {
        return "CommentStatus{" +
                "id=" + id +
                ", label='" + label + '\'' +
                '}';
    }

}
