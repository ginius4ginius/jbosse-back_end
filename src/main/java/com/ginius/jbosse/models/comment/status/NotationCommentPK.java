package com.ginius.jbosse.models.comment.status;

import java.io.Serializable;

public class NotationCommentPK implements Serializable {

    private String userEmail;
    private long statusId;
    private long commentId;

    public String getUserId() {
        return userEmail;
    }

    public void setUserId(String userEmail) {
        this.userEmail = userEmail;
    }

    public long getStatusId() {
        return statusId;
    }

    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public NotationCommentPK(){}

    public NotationCommentPK(String userEmail, long statusId, long commentId){
        this.userEmail = userEmail;
        this.statusId = statusId;
        this.commentId = commentId;
    }


}
