package com.ginius.jbosse.web;


import com.ginius.jbosse.dao.AnecdoteCategoryRepository;
import com.ginius.jbosse.models.anecdote.AnecdoteCategory;
import com.ginius.jbosse.utils.exception.CategoryNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class AnecdoteCategoryRestController {

    @Autowired
    AnecdoteCategoryRepository repository;

    private static String UPLOADED_FOLDER = "D:\\git\\repository\\jbosse\\jbosse-back_end\\documents\\photos\\anecdoteCategories\\";
    private static String UPLOADED_FOLDER_BACK = "D:\\git\\repository\\jbosse\\jbosse-back_end\\documents\\photos\\anecdoteCategoriesBack\\";

    private final Logger logger = Logger.getLogger(AnecdoteCategoryRestController.class);

    /**
     * Méthode permettant le retour des catégories des anecdotes (métiers)
     * @return liste des métiers
     */
    @GetMapping(value = "/categories")
    List<AnecdoteCategory> getAnecdoteCategories() {

        logger.debug("[getAnecdoteCategories] - Tentative de récupération des catégories");
        List<AnecdoteCategory> categories = repository.findAll();

        if (categories.isEmpty()) {
            logger.warn("[getAnecdoteCategories] - Pas de métiers dans la base");
            throw new CategoryNotFoundException(null, "Pas de métiers trouvés");
        }

        logger.debug("[getAnecdoteCategories] - Récupération des métiers réussie.");
        return categories;


    }

    /**
     * Méthode permettant le retour d'une catégorie d'un anecdote (métiers) par son label
     * @return liste des métiers
     */
    @GetMapping(value = "/category")
    AnecdoteCategory getAnecdoteCategory(@RequestParam(value = "label") String label) {

        logger.debug("[getAnecdoteCategory] - Tentative de récupération d'une catégorie");
        AnecdoteCategory category = repository.findAnecdoteCategoryByLabel(label);


        logger.debug("[getAnecdoteCategory] - Récupération des métiers réussie.");
        return category;


    }

    /**
     * Méthode récupérant la photo en base en format byte[] produisant une image jpg
     *
     * @param label de le catégorie
     * @return byte[] photo produit en format jpg
     */
    @GetMapping(value = "/photoAnecdoteCategory/{label}", produces = {MediaType.IMAGE_JPEG_VALUE,
            MediaType.IMAGE_PNG_VALUE})
    public byte[] getPhoto(@PathVariable("label") String label) {

        logger.debug("[getPhoto] - Tentative de récupération de la photo d'un métier.");
        AnecdoteCategory c = repository.findAnecdoteCategoryByLabel(label);

        byte[] file = new byte[0];

        try {
            file = Files.readAllBytes(Paths.get(UPLOADED_FOLDER
                    + c.getPhoto()));
        } catch (IOException e) {
            logger.warn("[getPhoto] - Erreur de récupération de la photo de du métier " + c.getLabel() + ": " + e.getMessage());
            try {
                logger.debug("[getPhoto] - Tentative de récupération de l'image par défaut");
                return Files.readAllBytes(Paths.get(UPLOADED_FOLDER
                        + "defaultAnecdoteCategorie.jpg"));
            } catch (IOException ex) {
                logger.warn("[getPhoto] - Erreur de récupération de la photo de du métier par defaut : " + e.getMessage());
            }
        }
        logger.debug("[getPhoto] - Récupération de la photo d'un métier réussie");

        return file;

    }

    /**
     * Méthode récupérant la photo en base en format byte[] produisant une image jpg
     *
     * @param label de le catégorie
     * @return byte[] photo produit en format jpg
     */
    @GetMapping(value = "/photoAnecdoteCategoryBack/{label}", produces = {MediaType.IMAGE_JPEG_VALUE,
            MediaType.IMAGE_PNG_VALUE})
    public byte[] getPhotoBack(@PathVariable("label") String label) {

        logger.debug("[getPhotoBack] - Tentative de récupération de la photo d'un métier.");
        AnecdoteCategory c = repository.findAnecdoteCategoryByLabel(label);

        byte[] file = new byte[0];

        try {
            file = Files.readAllBytes(Paths.get(UPLOADED_FOLDER_BACK
                    + c.getPhotoBack()));
        } catch (IOException e) {
            logger.warn("[getPhotoBack] - Erreur de récupération de la photo de du métier " + c.getLabel() + ": " + e.getMessage());
            try {
                logger.debug("[getPhoto] - Tentative de récupération de l'image par défaut");
                return Files.readAllBytes(Paths.get(UPLOADED_FOLDER_BACK
                        + "defaultAnecdoteCategorieBack.jpg"));
            } catch (IOException ex) {
                logger.warn("[getPhotoBack] - Erreur de récupération de la photo de du métier par defaut : " + e.getMessage());
            }
        }
        logger.debug("[getPhotoBack] - Récupération de la photo d'un métier réussie");

        return file;

    }

}
