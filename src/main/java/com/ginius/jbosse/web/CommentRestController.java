package com.ginius.jbosse.web;

import com.ginius.jbosse.dao.*;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.anecdote.Anecdote;
import com.ginius.jbosse.models.comment.Comment;
import com.ginius.jbosse.models.comment.CommentDto;
import com.ginius.jbosse.models.comment.status.CommentStatus;
import com.ginius.jbosse.models.comment.status.NotationComment;
import com.ginius.jbosse.utils.exception.AnecdoteNoteFoundException;
import com.ginius.jbosse.utils.exception.UserNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.toIntExact;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class CommentRestController {

    private final Logger logger = Logger.getLogger(CommentRestController.class);

    @Autowired
    CommentRepository repository;
    @Autowired
    AnecdoteRepository anecdoteRepository;
    @Autowired
    CommentStatusRepository commentStatusRepository;
    @Autowired
    MemberRepository memberRepository;
    @Autowired
    NotationCommentRepository notationCommmentrepository;

    @GetMapping(value = "/lastComments")
    private List<CommentDto> get5LastComments() {

        logger.debug("[get5LastComments] - Tentative de récupération des 5 derniers commentaires");
        Pageable paging = PageRequest.of(0, 5, Sort.by("publishedAt").descending());
        List<Comment> listeComments = repository.getComments(paging);
        List<CommentDto> commentDtos = new ArrayList<>();

        logger.debug("[get5LastComments] - Tentative de création des commentDto");
        for (Comment c : listeComments
        ) {

            List<List<NotationComment>> listeNotations = getAllNotations(c.getId());

            CommentDto cDto = new CommentDto(c.getId(), c.getDescription(), c.getPublishedAt(),c.getAnecdote(),
                    c.getUser(), listeNotations);
            commentDtos.add(cDto);
        }

        logger.debug("[get5LastComments] - Récupération réussie");
        return commentDtos;

    }

    /**
     * Méthode qui retourne une pagination des commentaires selon un anecdote cible
     * @param id anecdote lié aux anecdotes
     * @param size taille des pages a retounrer
     * @return une pagination d'anecdotes
     */
    @GetMapping(value = "/comments", params = {"id", "page","size"})
    public Page<CommentDto> getCommentsFromAnecdoteId(@RequestParam("page") int page,@RequestParam("id") long id, @RequestParam("size") int size) {

        logger.debug("[getCommentsFromAnecdoteId] - Tentative de récupération de l'anecdote par son id");
        Optional<Anecdote> anecdote = anecdoteRepository.findById(id);

        if(!anecdote.isPresent()){
            logger.warn("[getCommentsFromAnecdoteId] - Erreur, aucun anecdote trouvé avec id "+id);
            throw new AnecdoteNoteFoundException(id);
        }

        logger.debug("[getCommentsFromAnecdoteId] - Tentative de récupération des commentaires");
        List<Comment> commentaires = repository.getAllByAnecdote(anecdote.get());
        List<CommentDto> commentDtos = new ArrayList<>();

        logger.debug("[getCommentsFromAnecdoteId] - Tentative de création des commentDto");
        for (Comment c : commentaires
        ) {

            List<List<NotationComment>> listeNotations = getAllNotations(c.getId());

            CommentDto cDto = new CommentDto(c.getId(), c.getDescription(), c.getPublishedAt(),c.getAnecdote(),
                    c.getUser(), listeNotations);
            commentDtos.add(cDto);
        }

        logger.debug("[getCommentsFromAnecdoteId] - Génération de la pagination");
        PageRequest pageRequest = PageRequest.of(commentaires.size() / size, size);
        int total = commentaires.size();

        int start = toIntExact(pageRequest.getOffset());
        int end = Math.min((start + pageRequest.getPageSize()), total);

        List<CommentDto> output = new ArrayList<>();

        if (start <= end) {
            output = commentDtos.subList(start, end);
        }

        logger.debug("[getAnecdotesFromJCategory] - Pagination réussie");
        return new PageImpl<>(
                output,
                pageRequest,
                total
        );
    }

    /**
     * méthode d'insersion d'un status d'un commentaire
     *
     * @param status status de la notation
     * @param id identifiant de l'anecdote
     * @return une reponse
     */
    @PostMapping(value = "/commentStatus")
    public ResponseEntity<?> saveCommentStatus(@RequestParam("username") String username, @RequestParam("status") String status,
                                                @RequestParam("id") long id) {

        logger.debug("[saveCommentStatus] - Tentative de récupération du status...");
        Optional<CommentStatus> commentStatus = commentStatusRepository.getCommentStatusByLabel(status.toUpperCase());
        if (!commentStatus.isPresent()) {
            return new ResponseEntity<>("Erreur de sauvegarde du status du commentaire", HttpStatus.BAD_REQUEST);
        }

        logger.debug("[saveCommentStatus] - Tentative de récupération de l'utilisateur...");
        Optional<Member> member = memberRepository.findUserByUsername(username);

        if (!member.isPresent()) {
            return new ResponseEntity<>("Erreur de sauvegarde du status du commentaire", HttpStatus.BAD_REQUEST);
        }

        logger.debug("[saveCommentStatus] - Tentative de récupération du status du commentaire...");
        NotationComment notation = new NotationComment(member.get().getEmail(), commentStatus.get().getId(), id);
        List<NotationComment> notationEnBase = notationCommmentrepository.findAllByUserEmailAndCommentId(notation.getUserEmail(),id);
        if (!notationEnBase.isEmpty()) {
            return new ResponseEntity<>("L'utilisateur a déjà voté", HttpStatus.BAD_REQUEST);
        }

        logger.debug("[saveCommentStatus] - Tentative de sauvegarde du status du commentaire...");
        NotationComment result = notationCommmentrepository.save(notation);

        if (result == null) {
            logger.warn("[saveCommentStatus] - Erreur de sauvegarde du status du commentaire");
            return new ResponseEntity<>("Erreur de sauvegarde du status du commentaire", HttpStatus.BAD_REQUEST);
        } else {
            logger.debug("[saveCommentStatus] - Sauvegarde du status du commentaire réussie");
            return new ResponseEntity<>(result, HttpStatus.CREATED);
        }
    }

    /**
     * méthode d'insersion d'un commentaire
     *
     * @param c Comment
     * @return Comment
     */
    @PostMapping(value = "/comment")
    public Comment saveComment(@RequestBody Comment c) {

        c.setPublishedAt(LocalDateTime.now());

        logger.debug("[saveComment] - Tentative de récupération de l'utilisateur");

        Optional<Member> member = memberRepository.findUserByUsername(c.getUser().getUsername());

        if (!member.isPresent()) {
            throw new UserNotFoundException("[saveComment] - Utilisateur non trouvé");
        }

        logger.debug("[saveComment] - Tentative de récupération de l'anecdote");
        Optional<Anecdote> anecdote = anecdoteRepository.findById(c.getAnecdote().getId());

        if (!anecdote.isPresent()) {
            throw new AnecdoteNoteFoundException("[saveComment] - Anecdote non trouvée");
        }

        c.setAnecdote(anecdote.get());
        c.setUser(member.get());

        logger.debug("[saveComment] - Tentative de sauvegarde du commentaire");
        Comment comment = repository.save(c);

        logger.debug("[saveComment] - Sauvegarde du commentaire réussie");
        return comment;
    }

    /**
     * Méthode internet générant toutes les notations d'un commentaire
     * @param  commentId du commentaire
     * @return list de tableau de notation
     */
    private List<List<NotationComment>> getAllNotations(long commentId) {
        logger.debug("[getAllNotations] - Tentative de récupération des status");
        List<CommentStatus> listCommentStatus = commentStatusRepository.findAll();
        List<List<NotationComment>> listeNotations = new ArrayList<>();
        for (CommentStatus cs: listCommentStatus
        ) {
            listeNotations.add(notationCommmentrepository.findAllByCommentIdAndStatusId(commentId, cs.getId()));
        }

        return listeNotations;
    }

}
