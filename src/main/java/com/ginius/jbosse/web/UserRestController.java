package com.ginius.jbosse.web;

import com.ginius.jbosse.dao.RoleRepository;
import com.ginius.jbosse.dao.MemberRepository;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.Role;
import com.ginius.jbosse.models.UsersInfo;
import com.ginius.jbosse.utils.exception.UserNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.ginius.jbosse.utils.UtilsApi.generationUserRoles;

@CrossOrigin("*")
@RestController
public class UserRestController {

    private static String UPLOADED_FOLDER = "D:\\git\\repository\\jbosse\\jbosse-back_end\\documents\\photos\\utilisateurs\\";

    private final Logger logger = Logger.getLogger(UserRestController.class);

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private RoleRepository roleRepository;


    /**
     * retourne la liste des utilisateurs en BDD
     *
     * @return liste des utilisateurs
     */
    @GetMapping(value = "/account/users")
    public List<Member> getUsers() {
        logger.debug("[getUsers] - Tentative de récupération des utilisateurs");
        List<Member> members = memberRepository.findAll();

        if (members.isEmpty()) {
            logger.warn("[getUsers] - Pas d' utilisateurs dans la base");
            throw new UserNotFoundException(null, "Pas d'utilisateurs trouvés");
        }

        logger.debug("[getUsers] - Récupération des utilisateurs réussie");
        return members;

    }

    /**
     * méthode de récupération d'un utilisateur
     *
     * @param email de l'utilisateur
     * @return User
     */
    @GetMapping(value = "/account/user/{email}")
    public Member getUser(@PathVariable("email") String email) {

        logger.debug("[getUser] - Tentative de récupération du utilisateur: " + email);
        Member member = memberRepository.findUserByEmail(email).orElseThrow(() -> new UserNotFoundException(email));

        logger.debug("[getUser] - Récupération de l'utilisateur réussie");
        member.setgVersion(member.getgVersion() + 1);
        return member;
    }


    /**
     * méthode de récupération d'un utilisateur par son username
     *
     * @param username username
     * @return User
     */
    @GetMapping(value = "/account/userByUsername/{username}")
    public Member getUserByUsername(@PathVariable("username") String username) {

        logger.debug("[getUserByUsername] - Tentative de récupération de l' utilisateur: " + username);
        Member member = new Member();
        member = memberRepository.findUserByUsername(username).orElseThrow(() -> new UserNotFoundException(username));

        logger.debug("[getUserByUsername] - Récupération de l'utilisateur réussie");
        member.setgVersion(member.getgVersion() + 1);
        return member;
    }

    /**
     * Méthode récupérant la photo en base en format byte[] produisant une image jpg
     *
     * @param username de l'utilisateur
     * @return byte[] photo produit en format jpg
     */
    @GetMapping(value = "/photoUser/{username}", produces = {MediaType.IMAGE_JPEG_VALUE,
            MediaType.IMAGE_PNG_VALUE})
    public byte[] getPhoto(@PathVariable("username") String username) {

        logger.debug("[getPhoto] - Tentative de récupération de la photo de utilisateur " + username);

        Member u = memberRepository.findUserByUsername(username).orElseThrow(() -> new UserNotFoundException(username));

        byte[] file = new byte[0];

        try {
            System.out.println(UPLOADED_FOLDER
                    + "*******************************************defaultUser"+u.getSexe()+".PNG");
            file = Files.readAllBytes(Paths.get(UPLOADED_FOLDER
                    + u.getPhoto()));
        } catch (IOException e) {
            logger.debug("[getPhoto] - Erreur de récupération de la photo de l'utilisateur " + u.getUsername() + ": " + e.getMessage());
            try {
                logger.debug("[getPhoto] - Tentative de récupération de l'image par défaut");
                return Files.readAllBytes(Paths.get(UPLOADED_FOLDER
                        + "defaultUser"+u.getSexe()+".PNG"));
            } catch (IOException ex) {
                logger.warn("[getPhoto] - Erreur de récupération de la photo de l'utilisateur par defaut : " + e.getMessage());
            }
        }
        logger.debug("[getPhoto] - Récupération de la photo d'un utilisateur réussie");

        return file;

    }

    /**
     * Méthode qu ipermet la sauvegarde de la phot de l'utilisateur dans un répertoire de fichiers.
     *
     * @param file photo
     * @return message de retour de l'éxécution de la méthode.
     */
    @PostMapping(value = "/account/photoUser")
    public String uploadFile(@RequestParam("file") MultipartFile file) {

        logger.info(file.getOriginalFilename());

        logger.debug("[uploadFile] - Tentative de sauvegarde de la photo d'un utilisateur");
        if (file.isEmpty()) {
            logger.debug("[uploadFile] - Erreur de sauvegarde de la photo d'un utilisateur");
            return "{\"message\":\"erreur de sauvegarde de l'image\" }";
        }
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.debug("[uploadFile] - Sauvegarde de la photo d'un utilisateur réussie");
        return "{\"message\":\"succes\" }";
    }

    /**
     * méthode d'insersion d'un utilisateur
     *
     * @param u User
     * @return User
     */
    @PostMapping(value = "/account/user")
    public ResponseEntity<?> saveUser(@RequestBody Member u) {

        logger.debug("[saveUser] - Tentative de récupération du role utilisateur");
        Optional<Role> role = roleRepository.findRoleByLibelle("USER");

        if(!role.isPresent()){
            return new ResponseEntity<>("Erreur de sauvegarde de l'utilisateur", HttpStatus.BAD_REQUEST);
        }

        Collection<Role> roles = new ArrayList<>();
        roles.add(role.get());
        u.setRoles(roles);
        u.setDateCreation(LocalDateTime.now());

        logger.debug("[saveUser] - Mise à jour des informations de l'utilisateur");
        u.setNom(StringUtils.capitalize(u.getNom().toLowerCase()));
        u.setPrenom(StringUtils.capitalize(u.getPrenom().toLowerCase()));

        logger.debug("[saveUser] - Tentative de sauvegarde de l'utilisateur");
        Member member = memberRepository.save(u);

        if (member == null) {
            logger.warn("[saveUser] - Erreur de sauvegarde de l'utilisateur");
            return new ResponseEntity<>("Erreur de sauvegarde de l'utilisateur", HttpStatus.BAD_REQUEST);
        } else {
            logger.debug("[saveUser] - Sauvegarde de l'utilisateur réussie");
            return new ResponseEntity<>(member, HttpStatus.CREATED);
        }
    }

    /**
     * Méthode de mise à jour d'un utilisateur
     *
     * @param u User
     * @return User mis à jour ou user envoyé
     */
    @PutMapping(value = "/account/user")
    public Member updateUser(@RequestBody Member u) {

        logger.debug("[updateUser] - Tentative de mise à jour de l'utilisateur: " + u.getEmail());
        Member memberGeted = memberRepository.findUserByEmail(u.getEmail()).orElseThrow(() -> new UserNotFoundException(u.getEmail()));

        if (u.getPassword() != null && (!u.getPassword().equals(memberGeted.getPassword()))) {
            u.setPassword(bCryptPasswordEncoder.encode(u.getPassword()));
        } else {
            u.setPassword(memberGeted.getPassword());
        }

        u.setRoles(generationUserRoles(u.getRoles(), roleRepository.findAll()));

        logger.debug("[updateUser] - Les rôles de l'utilisateur sont : " + u.getRoles());

        u.setUsername(u.getUsername().toUpperCase());

        memberGeted.setPassword(u.getPassword());
        memberGeted.setUsername(u.getUsername());
        memberGeted.setNom(u.getNom());
        memberGeted.setPrenom(u.getPrenom());
        memberGeted.setPhoto(u.getPhoto());
        memberGeted.setRoles(u.getRoles());
        memberGeted.setActif(u.getActif());
        memberGeted.setSexe(u.getSexe());

        Member updatedMember = memberRepository.save(memberGeted);

        logger.debug("[updateUser] - Utilisateur mis à jour.");
        return updatedMember;
    }

    /**
     * Retourne une liste de pages de 10 utilisateurs
     *
     * @param mc   username par mc
     * @param page nombre de page
     * @param size taille de la page
     * @return object page
     */
    @RequestMapping(value = "/account/findUsers", method = RequestMethod.GET)
    public Page<Member> checkUsersByUserName(
            @RequestParam(name = "mc", defaultValue = "") String mc,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size) {
        return memberRepository.checkUsers("%" + mc + "%", PageRequest.of(page, size));
    }

    /**
     * Méthode retounant la liste des informations liées à l'ensemble des utilisateurs
     *
     * @return liste des informations
     */
    @GetMapping(value = "/account/usersInfo")
    public UsersInfo getUsersInfo() {
        logger.debug("[getUsersInfo] - Tentative de récupération des informations liés aux utilisateurs");
        UsersInfo usersInfo = memberRepository.getUsersInfo();

        if (usersInfo == null) {
            logger.warn("[getUsersInfo] - Erreur lors de la tentative de la récupération des informations des utilisateurs");

        }

        logger.debug("[getUsersInfo] - Récupération des informations liés aux utilisateurs réussie");
        return usersInfo;

    }


    /**
     * Méthode suppression d'un utilisateur retournant un boolean selon la réussite de l'éxécution.
     *
     * @param email email de l'utilisateur
     * @return boolean
     */
    @DeleteMapping(value = "/account/user/{email}")
    public boolean DeleteUser(@PathVariable("email") String email) {

        logger.debug("[DeleteUser] - Tentative de suppression de l'utilisateur: " + email);

        Member memberGeted = memberRepository.findUserByEmail(email).orElseThrow(() -> new UserNotFoundException(email));

        if (memberGeted == null) {
            logger.warn("[DeleteUser] - Utilisateur non trouvé avec l'email : " + email);
            throw new UserNotFoundException(null, "Aucun utilisateur trouvé avec l' email " + email);
        }

        memberRepository.delete(memberGeted);
        logger.debug("[DeleteUser] - Utilisateur supprimé avec succès");
        return true;


    }


}
