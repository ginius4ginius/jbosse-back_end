package com.ginius.jbosse.web;

import com.ginius.jbosse.dao.RoleRepository;
import com.ginius.jbosse.models.Role;
import com.ginius.jbosse.utils.exception.RoleNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/account")
public class RoleRestController {

    private final Logger logger = Logger.getLogger(RoleRestController.class);

    @Autowired
    private RoleRepository roleRepository;


    /**
     * méthode de récupération d'un role
     *
     * @param id id
     * @return Role
     */
    @GetMapping(value = "/role/{id}")
    public Role getRole(@PathVariable("id") Long id) {
        logger.debug("[getRole] - Tentative de récupération du role: " + id);
        Role role = roleRepository.findById(id).orElseThrow(() -> new RoleNotFoundException(id));
        role.setgVersion(role.getgVersion() + 1);


        logger.debug("[getRole] - Récupération du role réussie.");
        return role;
    }

    /**
     * retourne la liste des rôles en BDD
     * @return liste rôles
     */
    @GetMapping(value = "/roles")
    public List<Role> getRoles() {
        logger.debug("[getRoles] - Tentative de récupération des roles");
        List<Role> roles = roleRepository.findAll();

        if(roles.isEmpty()){
            logger.warn("[getRoles] - Pas de roles dans la base.");
            throw new RoleNotFoundException(null,"Pas de roles trouvés");
        }

        logger.debug("[getRoles] - Récupération des roles réussie.");
        return roles;

    }

    /**
     * méthode d'insersion d'un role
     *
     * @param r Role
     * @return Role
     */
    @PostMapping(value = "/role")
    public Role saveRole(@RequestBody Role r) {
        return roleRepository.save(r);
    }

}
