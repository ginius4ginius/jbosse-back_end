package com.ginius.jbosse.web;

import com.ginius.jbosse.dao.*;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.anecdote.Anecdote;
import com.ginius.jbosse.models.anecdote.AnecdoteCategory;
import com.ginius.jbosse.models.anecdote.AnecdoteDto;
import com.ginius.jbosse.models.anecdote.status.AnecdoteStatus;
import com.ginius.jbosse.models.anecdote.status.NotationAnecdote;
import com.ginius.jbosse.utils.exception.AnecdoteNoteFoundException;
import com.ginius.jbosse.utils.exception.UserNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.toIntExact;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class AnecdoteRestController {

    private final Logger logger = Logger.getLogger(AnecdoteRestController.class);

    @Autowired
    AnecdoteRepository repository;
    @Autowired
    AnecdoteCategoryRepository categoryRepository;
    @Autowired
    AnecdoteStatusRepository anecdoteStatusRepository;
    @Autowired
    NotationAnecdoteRepository notationAnecdoteRepository;
    @Autowired
    MemberRepository memberRepository;

    /**
     * Méthode de récupération des dernières anecdote par pagination
     *
     * @param page nombre de pages
     * @param size nombre dl'éléments par page
     * @return une liste
     */
    @GetMapping(value = "/lastAnecdotes", params = {"page", "size"})
    public Page<AnecdoteDto> getLastAnecdotes(@RequestParam("page") int page,
                                           @RequestParam("size") int size) {

        logger.debug("[getLastAnecdotes] - Tentative de récupération des 10 dernières anecdotes");
        PageRequest pageRequest = PageRequest.of(page, size);
        List<Anecdote> anecdotes = get10LastAnecdotes();
        List<AnecdoteDto> anecdoteDtos = new ArrayList<>();

        logger.debug("[getLastAnecdotes] - Tentative de création des AnecdoteDto");
        for (Anecdote a : anecdotes
        ) {

            List<List<NotationAnecdote>> listeNotations = getAllNotations(a.getId());

            AnecdoteDto aDto = new AnecdoteDto(a.getId(), a.getPublishedAt(), a.getDescription(), a.getCategory(),
                    a.getUser(), listeNotations, a.isactive());
            anecdoteDtos.add(aDto);
        }

        logger.debug("[getLastAnecdotes] - Génération de la pagination");
        int total = anecdotes.size();
        int start = toIntExact(pageRequest.getOffset());
        int end = Math.min((start + pageRequest.getPageSize()), total);

        List<AnecdoteDto> output = new ArrayList<>();

        if (start <= end) {
            output = anecdoteDtos.subList(start, end);
        }

        logger.debug("[getLastAnecdotes] - Pagination réussie");
        return new PageImpl<>(
                output,
                pageRequest,
                total
        );

    }

    private List<Anecdote> get10LastAnecdotes() {

        logger.debug("[get10LastAnecdotes] - Tentative de récupération des 10 dernières anecdotes");
        Pageable paging = PageRequest.of(0, 10, Sort.by("publishedAt").descending());
        List<Anecdote> listeAnecdotes = repository.getActiveAnecdotes(paging);

        logger.debug("[get10LastAnecdotes] - Récupération réussie");
        return listeAnecdotes;

    }

    /**
     * Méthode de récupération des dernières anecdote par pagination
     *
     * @param page nombre de pages
     * @param size nombre dl'éléments par page
     * @return une liste
     */
    @GetMapping(value = "/bestAnecdotes", params = {"page", "size"})
    public Page<AnecdoteDto> getBestAnecdotes(@RequestParam("page") int page,
                                              @RequestParam("size") int size) {

        logger.debug("[getBestAnecdotes] - Tentative de récupération des 10 meilleurs anecdotes");
        PageRequest pageRequest = PageRequest.of(page, size);
        List<Anecdote> anecdotes = get10BestAnecdotes();
        List<AnecdoteDto> anecdoteDtos = new ArrayList<>();

        logger.debug("[getBestAnecdotes] - Tentative de création des AnecdoteDto");
        for (Anecdote a : anecdotes
        ) {

            List<List<NotationAnecdote>> listeNotations = getAllNotations(a.getId());

            AnecdoteDto aDto = new AnecdoteDto(a.getId(), a.getPublishedAt(), a.getDescription(), a.getCategory(),
                    a.getUser(), listeNotations, a.isactive());
            anecdoteDtos.add(aDto);
        }

        logger.debug("[getBestAnecdotes] - Génération de la pagination");
        int total = anecdoteDtos.size();
        int start = toIntExact(pageRequest.getOffset());
        int end = Math.min((start + pageRequest.getPageSize()), total);

        List<AnecdoteDto> output = new ArrayList<>();

        if (start <= end) {
            output = anecdoteDtos.subList(start, end);
        }

        logger.debug("[getBestAnecdotes] - Pagination réussie");
        return new PageImpl<>(
                output,
                pageRequest,
                total
        );

    }

    /**
     * Méthode internet générant toutes les notations d'une anecdote
     * @param  anecdoteId de l'anecdote
     * @return list de tableau de notation
     */
    private List<List<NotationAnecdote>> getAllNotations(long anecdoteId) {
        logger.debug("[getAllNotations] - Tentative de récupération des status");
        List<AnecdoteStatus> listAnecdoteStatus = anecdoteStatusRepository.findAll();
        List<List<NotationAnecdote>> listeNotations = new ArrayList<>();
        for (AnecdoteStatus as: listAnecdoteStatus
             ) {
            listeNotations.add(notationAnecdoteRepository.findAllByAnecdoteIdAndStatusId(anecdoteId, as.getId()));
        }

        return listeNotations;
    }

    private List<Anecdote> get10BestAnecdotes() {

        logger.debug("[get10LastAnecdotes] - Tentative de récupération des 10 dernières anecdotes");
        Pageable paging = PageRequest.of(0, 10);
        List<Anecdote> listeAnecdotes = repository.getBestAnecdotes(paging);

        logger.debug("[get10LastAnecdotes] - Récupération réussie");
        return listeAnecdotes;

    }

    /**
     * Méthode qui retourne une pagination des anecdotes selon un métier cible
     *
     * @param category métier lié aux anecdotes
     * @param size     taille des pages a retounrer
     * @return une pagination d'anecdotes
     */
    @GetMapping(value = "/anecdotes", params = {"job","page","size"})
    public Page<AnecdoteDto> getAnecdotesFromCategory(@RequestParam("job") String category,
                                                      @RequestParam("page") int page, @RequestParam("size") int size) {

        logger.debug("[getAnecdotesFromCategory] - Tentative de récupération de la catégorie de l'anecdote");
        AnecdoteCategory cat = categoryRepository.findAnecdoteCategoryByLabel(category);

        logger.debug("[getAnecdotesFromCategory] - Tentative de récupération des anecdotes");
        List<Anecdote> anecdotes = repository.getAllByCategoryAndActiveIsTrue(cat);

        List<AnecdoteDto> anecdoteDtos = new ArrayList<>();

        logger.debug("[getAnecdotesFromCategory] - Tentative de création des AnecdoteDto");
        for (Anecdote a : anecdotes
        ) {

            List<List<NotationAnecdote>> listeNotations = getAllNotations(a.getId());

            AnecdoteDto aDto = new AnecdoteDto(a.getId(), a.getPublishedAt(), a.getDescription(), a.getCategory(),
                    a.getUser(), listeNotations, a.isactive());
            anecdoteDtos.add(aDto);
        }

        logger.debug("[getAnecdotesFromCategory] - Génération de la pagination");
        PageRequest pageRequest = PageRequest.of(page, size);

        int total = anecdoteDtos.size();
        int start = toIntExact(pageRequest.getOffset());
        int end = Math.min((start + pageRequest.getPageSize()), total);

        List<AnecdoteDto> output = new ArrayList<>();

        if (start <= end) {
            output = anecdoteDtos.subList(start, end);
        }

        logger.debug("[getAnecdotesFromCategory] - Pagination réussie");
        return new PageImpl<>(
                output,
                pageRequest,
                total
        );
    }

    /**
     * Méthode qui retourne un anecdote par son id
     *
     * @param id id de l'anecdote
     * @return un object anecdote
     */
    @GetMapping(value = "/anecdote", params = "id")
    public AnecdoteDto getAnecdoteById(@RequestParam("id") long id) {
        logger.debug("[getAnecdoteById] - Tentative de récupération de  l'anecdote");
        Optional<Anecdote> a = repository.findById(id);

        if (!a.isPresent()) {
            logger.warn("[getAnecdoteById] - Erreur, aucun anecdote trouvé avec id " + id);
            throw new AnecdoteNoteFoundException(id);
        }

        logger.debug("[getAnecdoteById] - Tentative de création des AnecdoteDto");

            List<List<NotationAnecdote>> listeNotations = getAllNotations(a.get().getId());

            AnecdoteDto aDto = new AnecdoteDto(a.get().getId(), a.get().getPublishedAt(), a.get().getDescription(),
                    a.get().getCategory(), a.get().getUser(), listeNotations, a.get().isactive());

        logger.debug("[getAnecdoteById] - Récupération de  l'anecdote réussie");

        return aDto;

    }

    /**
     * méthode d'insersion d'un status d'un anecdote
     *
     * @param status status de la notation
     * @param id     identifiant de l'anecdote
     * @return une reponse
     */
    @PostMapping(value = "/anecdoteStatus")
    public ResponseEntity<?> saveAnecdoteStatus(@RequestParam("username") String username, @RequestParam("status") String status,
                                                @RequestParam("id") long id) {

        logger.debug("[saveAnecdoteStatus] - Tentative de récupération du status...");
        Optional<AnecdoteStatus> anecdoteStatus = anecdoteStatusRepository.getAnecdoteStatusByLabel(status.toUpperCase());
        if (!anecdoteStatus.isPresent()) {
            return new ResponseEntity<>("Erreur de sauvegarde du status de l'anecdote", HttpStatus.BAD_REQUEST);
        }

        logger.debug("[saveAnecdoteStatus] - Tentative de récupération de l'utilisateur...");
        Optional<Member> member = memberRepository.findUserByUsername(username);

        if (!member.isPresent()) {
            return new ResponseEntity<>("Erreur de sauvegarde du status de l'anecdote", HttpStatus.BAD_REQUEST);
        }

        logger.debug("[saveAnecdoteStatus] - Tentative de récupération du status à l'anecdote...");
        NotationAnecdote notation = new NotationAnecdote(member.get().getEmail(), anecdoteStatus.get().getId(), id);
        List<NotationAnecdote> notationEnBase = notationAnecdoteRepository.findAllByUserEmailAndAnecdoteId(notation.getUserEmail(), id);
        if (!notationEnBase.isEmpty()) {
            return new ResponseEntity<>("L'utilisateur a déjà voté", HttpStatus.BAD_REQUEST);
        }

        logger.debug("[saveAnecdoteStatus] - Tentative de sauvegarde du status à l'anecdote...");
        NotationAnecdote result = notationAnecdoteRepository.save(notation);

        if (result == null) {
            logger.warn("[saveAnecdoteStatus] - Erreur de sauvegarde du status de l'anecdote");
            return new ResponseEntity<>("Erreur de sauvegarde du status de l'anecdote", HttpStatus.BAD_REQUEST);
        } else {
            logger.debug("[saveAnecdoteStatus] - Sauvegarde du status de l'anecdote réussie");
            return new ResponseEntity<>(result, HttpStatus.CREATED);
        }
    }

    /**
     * méthode d'insersion d'un anecdote
     *
     * @param a Anecdote
     * @return Anecdote
     */
    @PostMapping(value = "/anecdote")
    public Anecdote saveAnecdote(@RequestBody Anecdote a) {

        a.getUser().setDateCreation(LocalDateTime.now());

        logger.debug("[saveAnecdote] - Tentative de récupération de l'utilisateur");

        Optional<Member> member = memberRepository.findUserByUsername(a.getUser().getUsername());

        if (!member.isPresent()) {
            throw new UserNotFoundException("[saveAnecdote] - Utilisateur non trouvé");
        }

        logger.debug("[saveAnecdote] - Tentative de récupération de la catégorie de l'anecdote");
        Optional<AnecdoteCategory> cat = categoryRepository.findById(a.getCategory().getId());

        if (!cat.isPresent()) {
            throw new AnecdoteNoteFoundException("[saveAnecdote] - Catégorie de l'anecdote non trouvée");
        }

        Anecdote anecdote = new Anecdote();
        anecdote.setPublishedAt(LocalDateTime.now());
        anecdote.setUser(member.get());
        anecdote.setCategory(cat.get());
        anecdote.setactive(true);
        anecdote.setDescription(a.getDescription());

        logger.debug("[saveAnecdote] - Tentative de sauvegarde de l'anecdote");
        Anecdote result = repository.save(anecdote);

        logger.debug("[saveAnecdote] - Sauvegarde de l'anecdote réussie");
        return result;
    }

}
