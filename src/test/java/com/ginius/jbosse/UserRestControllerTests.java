package com.ginius.jbosse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ginius.jbosse.dao.RoleRepository;
import com.ginius.jbosse.models.Member;
import com.ginius.jbosse.models.Role;
import com.ginius.jbosse.models.Sexe;
import com.test.annotation.type.UnitTest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collection;
import java.util.regex.Pattern;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserRestControllerTests {

    private String token;

    private static final String USER_PATH = "/account/";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RoleRepository roleRepository;

    @Before
    public void test0_whenLogin_thenStatusIsOk() throws Exception {
        MvcResult result = this.mockMvc.perform(post("/login").content("{\"username\": \"ginius\", \"password\": \"1234\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
        token = result.getResponse().getHeader("Authorization");
    }

    @Test
    public void test1_whenReadOneById_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "user/ginius4@hotmail.com"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test2_whenReadOneById_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "user/ginius4@hotmail.com")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test3_whenReadOneById_thenStatusIsNotFound() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "user/ginius5@hotmail.com")
                .header("Authorization", token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test4_whenReadOneByUsername_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "userByUsername/bouh20")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test5_whenReadOneByUsername_thenStatusIsNotFound() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "userByUsername/bouh50")
                .header("Authorization", token))
                .andExpect(status().isNotFound());
    }


    @Test
    public void test6_whenCreate_thenStatusIsCreated() throws Exception {

        Collection<Role> roles = roleRepository.findAll();
        Member member = new Member("ginius5@hotmail.com", "ginius5", "1234", "derieu", "dédé",
                true, "ginius5.jpg", Sexe.HOMME, roles);
        this.mockMvc.perform(post(USER_PATH + "user").header("Authorization", token)
                .content(asJsonString(member))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void test7_whenUpdate_thenStatusIsOk() throws Exception {

        Collection<Role> roles = roleRepository.findAll();
        Member member = new Member("ginius5@hotmail.com", "ginius50", "123456", "derieu", "dédé",
                true, "ginius50.jpg", Sexe.HOMME, roles);
        System.out.println("***************************");
        System.out.println(member);
        System.out.println("***************************");
        this.mockMvc.perform(put(USER_PATH + "user")
                .header("Authorization", token)
                .content(asJsonString(member))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void test8_whenDelete_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(delete(USER_PATH + "user/ginius5@hotmail.com")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    private String asJsonString(final Object obj) {
        try {
            String result = new ObjectMapper().writeValueAsString(obj);

            return Pattern.compile("\"dateCreation\":.+}}").matcher(result)
                    .replaceAll("\"dateCreation\": \"2020-04-17T09:12:38\"");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
