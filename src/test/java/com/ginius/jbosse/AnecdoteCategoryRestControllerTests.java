package com.ginius.jbosse;

import com.test.annotation.type.UnitTest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AnecdoteCategoryRestControllerTests {

    private String token;

    private static final String COMMENT_PATH = "/api/";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void test0_whenLogin_thenStatusIsOk() throws Exception {
        MvcResult result = this.mockMvc.perform(post("/login").content("{\"username\": \"ginius\", \"password\": \"1234\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
        token = result.getResponse().getHeader("Authorization");
    }

    @Test
    public void test1_whenReadAnecdoteCategoryByLabel_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "category?label=artisanat"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test2_whenReadAnecdoteCategoryByLabel_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "category?label=artisanat")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test3_whenGetPhotoAnecdoteCategory_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "photoAnecdoteCategory/artisanat"))
                .andExpect(status().isOk());
    }

    @Test
    public void test4_whenGetPhotoAnecdoteCategory_thenStatusIsNotFound() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "photoAnecdoteCategory/restauration")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test5_whenGetPhotoBackAnecdoteCategory_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "photoAnecdoteCategoryBack/artisanat"))
                .andExpect(status().isOk());
    }

    @Test
    public void test6_whenGetPhotoBackAnecdoteCategory_thenStatusIsNotFound() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "photoAnecdoteCategoryBack/restauration")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

}
