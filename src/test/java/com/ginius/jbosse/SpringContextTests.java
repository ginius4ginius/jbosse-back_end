package com.ginius.jbosse;

import com.test.annotation.type.UnitTest;
import org.junit.experimental.categories.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {JbosseApplication.class})
@Category(UnitTest.class)
public class SpringContextTests {

	@Test
	public void contextLoads() {
	}

}
