package com.ginius.jbosse;

import com.test.annotation.type.UnitTest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommentRestControllerTests {

    private String token;

    private static final String COMMENT_PATH = "/api/";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void test0_whenLogin_thenStatusIsOk() throws Exception {
        MvcResult result = this.mockMvc.perform(post("/login").content("{\"username\": \"ginius\", \"password\": \"1234\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
        token = result.getResponse().getHeader("Authorization");
    }

    @Test
    public void test1_whenReadLastComments_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "lastComments"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test2_whenReadLastComments_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "lastComments")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test3_whenReadComments_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "comments?id=1&page=1&size=5"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test4_whenReadComments_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "comments?id=1&page=1&size=5")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

}
