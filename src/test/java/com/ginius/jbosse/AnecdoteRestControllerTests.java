package com.ginius.jbosse;

import com.test.annotation.type.UnitTest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AnecdoteRestControllerTests {

    private String token;

    private static final String COMMENT_PATH = "/api/";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void test0_whenLogin_thenStatusIsOk() throws Exception {
        MvcResult result = this.mockMvc.perform(post("/login").content("{\"username\": \"ginius\", \"password\": \"1234\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
        token = result.getResponse().getHeader("Authorization");
    }

    @Test
    public void test1_whenReadLastAnecdotes_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "lastAnecdotes?page=1&size=10"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test2_whenReadLastAnecdotes_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "lastAnecdotes?page=1&size=10")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test3_whenReadBestAnecdotes_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "bestAnecdotes?page=1&size=10"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test4_whenReadBestAnecdotes_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "bestAnecdotes?page=1&size=10")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test5_whenGetAnecdoteById_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "anecdote?id=1"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test6_whenGetAnecdoteById_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(COMMENT_PATH + "anecdote?id=1")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

}
