package com.ginius.jbosse;

import com.test.annotation.type.UnitTest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SecurityTests {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void test0_whenLoginWithGoodIdentifiers() throws Exception {

        this.mockMvc.perform(post("/login")
                .content("{\"username\": \"ginius\", \"password\": \"1234\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

    }

    @Test
    public void test1_whenLoginWithBadIdentifiers() throws Exception {
        this.mockMvc.perform(post("/login")
                .content("{\"username\": \"ginius\", \"password\": \"123456\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(401));
    }

    @Test
    public void test2_whenLoginGenerateAccessToken() throws Exception {
        this.mockMvc.perform(post("/login")
                .content("{\"username\": \"ginius\", \"password\": \"1234\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(header().exists("Authorization"));
    }


}
