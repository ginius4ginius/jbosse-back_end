package com.ginius.jbosse;

import com.test.annotation.type.UnitTest;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RoleRestControllerTests {

    private String token;

    private static final String USER_PATH = "/account/";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void test0_whenLogin_thenStatusIsOk() throws Exception {
        MvcResult result = this.mockMvc.perform(post("/login").content("{\"username\": \"ginius\", \"password\": \"1234\"}\n")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();
        token = result.getResponse().getHeader("Authorization");
    }

    @Test
    public void test1_whenReadOneById_thenStatusIsKoWithoutToken() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "role/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void test2_whenReadOneById_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "role/1")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

    @Test
    public void test3_whenReadOneById_thenStatusIsNotFound() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "role/10")
                .header("Authorization", token))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test3_whenFindALl_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(USER_PATH + "roles")
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

}
